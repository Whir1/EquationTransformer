﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EquationTransformer.Lib.Parsers;
using EquationTransformer.Lib.Processors;
using FluentAssertions;
using NUnit.Framework;

namespace EquationTransformer.Lib.Tests
{
    [TestFixture]
    public class EquationConverterFactoryFixture
    {
        private EquationConverterFactory _target;

        [SetUp]
        public void SetUp()
        {
            _target = new EquationConverterFactory();
        }

        [Test]
        public void WhenBuildObject_ThenReturnsExpected()
        {
            var actual = _target.BuildEquationConverter(CultureInfo.InvariantCulture);

            Assert.IsInstanceOf<EquationConverter>(actual);
        }        
    }
}