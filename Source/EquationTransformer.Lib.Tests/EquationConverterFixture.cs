﻿using System.Collections.Generic;
using EquationTransformer.Lib.Model;
using EquationTransformer.Lib.Parsers;
using EquationTransformer.Lib.Processors;
using Moq;
using NUnit.Framework;

namespace EquationTransformer.Lib.Tests
{
    [TestFixture]
    public class EquationConverterFixture
    {
        private EquationConverter _target;
        private List<IEquationProcessor> _processors;
        private Mock<IEquationProcessor> _mockProcessor;
        private Mock<IParser<Equation>> _mockParser;

        [SetUp]
        public void SetUp()
        {
            _mockProcessor = new Mock<IEquationProcessor>();
            _mockParser = new Mock<IParser<Equation>>();
            _processors = new List<IEquationProcessor>() { _mockProcessor.Object };

            _target = new EquationConverter(_mockParser.Object, _processors);
        }

        [Test]
        public void WhenCallConverToCanonicat_ThenParseAndCallProcess()
        {
            var testEq = new Equation(new[] {SummandHelper.GetZeroSummand()}, new[] {SummandHelper.GetZeroSummand()});

            _mockParser.Setup(c => c.Parse(It.IsAny<string>())).Returns(testEq);

            _mockProcessor.Setup(c => c.ProcessEquation(It.IsAny<Equation>())).Returns(testEq);

            var actual = _target.ConvertToCanonical("asdqwe");

            Assert.AreEqual("0=0", actual);
            _mockProcessor.Verify(c => c.ProcessEquation(It.IsAny<Equation>()), Times.Once);
            _mockParser.Verify(c =>c .Parse("asdqwe"), Times.Once);
        }
         
    }
}