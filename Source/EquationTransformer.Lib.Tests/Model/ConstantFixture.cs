﻿using System.Globalization;
using EquationTransformer.Lib.Model;
using FluentAssertions;
using NUnit.Framework;

namespace EquationTransformer.Lib.Tests.Model
{
    [TestFixture]
    public class ConstantFixture
    {
        [Test]
        [TestCase(0f)]
        [TestCase(1f)]
        [TestCase(2f)]
        [TestCase(2.1f)]
        [TestCase(-1f)]
        [TestCase(-5.7f)]
        public void WhenCallToString_ThenReturnsValueToString(float value)
        {
            string expected = value.ToString(CultureInfo.InvariantCulture);

            var target = new Constant(value);

            string actual = target.ToString();

            Assert.IsFalse(target.IsComplex);
            Assert.AreEqual(target.Variabletype, VariableType.Constant);
            Assert.AreEqual(expected, actual);
        }

    }
}