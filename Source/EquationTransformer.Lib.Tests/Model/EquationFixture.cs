﻿using System;
using System.Collections.Generic;
using EquationTransformer.Lib.Model;
using NUnit.Framework;

namespace EquationTransformer.Lib.Tests.Model
{
    [TestFixture]
    public class EquationFixture
    {
        [Test]
        [TestCaseSource(nameof(EquationTestCases))]
        public void WhenCallToString_ThenReturnsExpectedResult(List<Summand> left, List<Summand> right, string expected)
        {
            var target = new Equation(left, right);

            var actual = target.ToString();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void PartIsNullOrEmptyThenThrowsArgumentExceptions()
        {
            Assert.Throws<ArgumentException>(() => new Equation(null, null));
            Assert.Throws<ArgumentException>(() => new Equation(new List<Summand>(), null));
            Assert.Throws<ArgumentException>(() => new Equation(null, new List<Summand>()));
            Assert.Throws<ArgumentException>(() => new Equation(new List<Summand>(), new List<Summand>()));

        }

        private static readonly object[] EquationTestCases = {
            new object[] { new List<Summand> { ConstSummand(), VariableSummand(), NegativeSummand()}, new List<Summand> { ConstSummand()}, "5+x-5y=5"},
            new object[] { new List<Summand> { ConstSummand(), ComplexSummand(), NegativeSummand()}, new List<Summand> { ConstSummand()}, "5+5xy^2-5y=5"},
            new object[] { new List<Summand> { ConstSummand(), VariableSummand(), ComplexSummand() }, new List<Summand> { ComplexSummand() }, "5+x+5xy^2=5xy^2"},
        };


        private static Summand ConstSummand()
        {
            return new Summand(new List<IVariable> {new Constant(5)});
        }

        private static Summand VariableSummand()
        {
            return new Summand(new[] {new Variable('x') });
        }

        private static Summand NegativeSummand()
        {
            return new Summand(new List<IVariable>() {new Constant(-5), new Variable('y')});
        }

        private static Summand ComplexSummand()
        {
            return new Summand(new List<IVariable> { new Constant(5), new Variable('x'), new Variable('y', 2) });
        }
    }
}