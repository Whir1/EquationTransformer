﻿using System;
using System.Collections.Generic;
using EquationTransformer.Lib.Model;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace EquationTransformer.Lib.Tests.Model
{
    [TestFixture]
    public class SummandFixture
    {
        [Test]
        public void WheCallToStringEmptySummand_ThenThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new Summand(null));
            Assert.Throws<ArgumentException>(() => new Summand(new List<IVariable>()));
        }

        [Test]
        public void WhenWhaveConstantsAndComplexVariables_ThenToStringReturnsExpected()
        {
            var expected = "5*5xy^2";

            var target = new Summand(new List<IVariable>()
            {
                new Constant(5),
                new Constant(5),
                new Variable('x'),
                new Variable('y', 2f)
            });

            string actual = target.ToString();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void WhenWhaveConstantsAndSimpleVariables_ThenToStringReturnsExpected()
        {
            var expected = "5*5xy";

            var target = new Summand(new List<IVariable>()
            {
                new Constant(5),
                new Constant(5),
                new Variable('x'),
                new Variable('y')
            });

            string actual = target.ToString();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void WhenWhaveOnlyVariables_ThenToStringReturnsExpected()
        {
            var expected = "xy";

            var target = new Summand(new List<IVariable>
            {
                new Variable('x'),
                new Variable('y')
            });

            string actual = target.ToString();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void WhenWhaveOnlyConstants_ThenToStringReturnsExpected()
        {
            var expected = "5*6";

            var target = new Summand(new List<IVariable>()
            {
                new Constant(5),
                new Constant(6)
            });

            string actual = target.ToString();

            Assert.AreEqual(expected, actual);
        }

    }
}