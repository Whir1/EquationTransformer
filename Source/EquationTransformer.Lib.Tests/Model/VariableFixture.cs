﻿using EquationTransformer.Lib.Model;
using NUnit.Framework;

namespace EquationTransformer.Lib.Tests.Model
{
    [TestFixture]
    public class VariableFixture
    {
        [Test]
        public void WhenPowerIsZero_ThenReturns0()
        {
            var giwen = new Variable('x', 0);

            string actual = giwen.ToString();

            Assert.AreEqual(actual, "1");
            Assert.IsFalse(giwen.IsComplex);
        }

        [Test]
        public void WhenPowerIsOne_ThenReturnsName()
        {
            var giwen = new Variable('x');

            string actual = giwen.ToString();

            Assert.AreEqual(actual, "x");
            Assert.IsFalse(giwen.IsComplex);
        }

        [Test]
        public void WhenPowerMoreThenOne_ThenToStringReturnsExpectedResult()
        {
            var giwen = new Variable('x', 2);

            string actual = giwen.ToString();

            Assert.AreEqual(actual, "x^2");
            Assert.IsTrue(giwen.IsComplex);

        }
    }
}