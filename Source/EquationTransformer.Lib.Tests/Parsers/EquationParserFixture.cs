﻿using EquationTransformer.Lib.Model;
using EquationTransformer.Lib.Parsers;
using Moq;
using NUnit.Framework;

namespace EquationTransformer.Lib.Tests.Parsers
{
    [TestFixture]
    public class EquationParserFixture
    {
        private EquationParser _target;
        private Mock<IParser<Summand>> _summandParser;


        [SetUp]
        public void SetUp()
        {
            _summandParser = new Mock<IParser<Summand>>();
            _summandParser.Setup(c => c.Parse(It.IsAny<string>())).Returns(SummandHelper.GetZeroSummand());

            _target = new EquationParser(_summandParser.Object);
        }

        [Test]
        [TestCase("x+y=0", 2, 1)]
        [TestCase("x^2+2y=5x-7", 2, 2)]
        [TestCase("5x^2+xy+7x^2y^2=9x-7+1y", 3, 3)]
        [TestCase("5x^2 + xy +7x ^ 2y ^ 2=9x-7+1y", 3, 3)]
        public void CanParseStringAndSmallAssert(string equationStr, int leftPartCount, int rightPartCount)
        {
            Equation actual = _target.Parse(equationStr);

            Assert.AreEqual(leftPartCount, actual.LeftParts.Count);
            Assert.AreEqual(rightPartCount, actual.RightParts.Count);
        }

    }
}