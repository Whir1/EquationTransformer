﻿using System.Globalization;
using EquationTransformer.Lib.Model;
using EquationTransformer.Lib.Parsers;
using NUnit.Framework;

namespace EquationTransformer.Lib.Tests.Parsers
{
    [TestFixture]
    public class SummandParserFixture
    {
        [Test]
        public void WhenParseString_ThenReturnsExpectedSummnd()
        {
            var giwen = "52*x*(y^-2)*(z^2)";
            var expected = "52x(y^-2)(z^2)";

            SummandParser target = new SummandParser(CultureInfo.InvariantCulture);

            Summand actual = target.Parse(giwen);

            Assert.AreEqual(expected, actual.ToString());
        }
    }
}