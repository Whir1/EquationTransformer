﻿using System.Collections.Generic;
using System.Linq;
using EquationTransformer.Lib.Model;
using EquationTransformer.Lib.Processors;
using FluentAssertions;
using NUnit.Framework;

namespace EquationTransformer.Lib.Tests.Processors
{
    [TestFixture]
    public class PartMoveProcessorFixture
    {
        private PartMoveProcessor _target;
        private List<Summand> _leftParts;
        private List<Summand> _rightParts;

        [SetUp]
        public void SetUp()
        {
            _target = new PartMoveProcessor();

            _leftParts = new List<Summand>();
            _rightParts = new List<Summand>();
        }

        [Test]
        public void WhenHasRightPart_ThenMoveItToLeftWithAdditionalVariable()
        {

            _leftParts.Add(SummandHelper.GetZeroSummand());
            _rightParts.Add(new Summand(new [] {new Constant(5) }));

            var expectedLeft = new List<Summand>(_leftParts);
            foreach (var s in _rightParts)
            {
                var v = s.Variables.ToList();
                v.Add(new Constant(-1));
                expectedLeft.Add( new Summand(v));
            }
            var expectedRight = new List<Summand> { SummandHelper.GetZeroSummand()};
            var expected = new Equation(expectedLeft, expectedRight);


            var giwen = new Equation(_leftParts, _rightParts);

            var actual = _target.ProcessEquation(giwen);

            actual.ShouldBeEquivalentTo(expected);
        }
    }
}