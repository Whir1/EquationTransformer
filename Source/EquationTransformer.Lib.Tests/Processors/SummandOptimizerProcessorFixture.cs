﻿using System.Collections.Generic;
using EquationTransformer.Lib.Model;
using EquationTransformer.Lib.Processors;
using NUnit.Framework;

namespace EquationTransformer.Lib.Tests.Processors
{
    [TestFixture]
    public class SummandOptimizerProcessorFixture
    {
        [Test]
        [TestCaseSource(nameof(TestCases))]
        public void WhenOptimizeSummands_ThenResultSholdBeEqualsToExpected(List<Summand> summands, string expected )
        {
            var target = new SummandOptimizerProcessor();

            var giwen = new Equation(summands, new List<Summand>() {SummandHelper.GetZeroSummand()});

            Equation actual = target.ProcessEquation(giwen);

            Assert.AreEqual(expected, actual.ToString());
        }

        private static readonly object[] TestCases = {
            new object[]
            {
                new List<Summand>()
                {
                    new Summand(new List<IVariable>
                    {
                        new Constant(1),
                        new Variable('x'),
                        new Constant(5),
                    })   
                } ,
                "5x=0"
            },
            new object[]
            {
                new List<Summand>
                {
                    new Summand(new List<IVariable>
                    {
                        new Constant(1),
                        new Variable('x'),
                        new Constant(5),
                    }),
                    new Summand(new List<IVariable>()
                    {
                        new Variable('y', 2),
                        new Variable('y'),
                        new Constant(5),
                        new Variable('x', 5),
                        new Constant(7),
                        new Constant(-1)
                    })
                } ,
                "5x-35(y^3)(x^5)=0"
            },
        };
    }
}