﻿using System.Collections.Generic;
using EquationTransformer.Lib.Model;
using EquationTransformer.Lib.Processors;
using NUnit.Framework;

namespace EquationTransformer.Lib.Tests.Processors
{
    [TestFixture]
    public class SummandReducerProcessorFixture
    {
        [Test]
        [TestCaseSource(nameof(TestCases))]
        public void WhenReduseSummands_ThenResultShouldBeEqualsToExpected(List<Summand> summands, string expected)
        {
            var target = new SummandReducerProcessor();

            var giwen = new Equation(summands, new List<Summand>() { SummandHelper.GetZeroSummand() });

            Equation actual = target.ProcessEquation(giwen);

            Assert.AreEqual(expected, actual.ToString());

        }

        private static readonly object[] TestCases = {
            new object[]
            {
                new List<Summand>()
                {
                    new Summand(new List<IVariable>
                    {
                        new Constant(5),
                        new Variable('x'),
                    }),
                    new Summand(new List<IVariable>
                    {
                        new Constant(5),
                        new Variable('x'),
                    })
                } ,
                "10x=0"
            },
            new object[]
            {
                new List<Summand>
                {
                    new Summand(new List<IVariable>
                    {
                        new Constant(5),
                        new Variable('x',2),
                        new Variable('y'),
                    }),
                    new Summand(new List<IVariable>()
                    {
                        new Variable('x', 2),
                        new Variable('y'),
                    })
                } ,
                "6yx^2=0"
            },
        };
    }
}