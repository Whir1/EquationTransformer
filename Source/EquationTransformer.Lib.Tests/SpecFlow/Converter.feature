﻿Feature: Converter
	Common test for parsing end transform to canonical

Background: 
	Given Create converter

@Integrate
Scenario Outline: Convert equation
	Given I have entered string equation <parsingEquation>
	When I convert equation
	Then the result should be <expectedString>

Examples: 
| parsingEquation                 | expectedString     |
#| x=5                             | x-5=0              |
#| 5x^2=5                          | 5x^2-5=0           |
#| 7*8*y^3 = 10 +8.5(5x^2)         | 56y^3-10-42.5x^2=0 |
#| 10x*23y-1.5xy+7y^2=5x-7y^2+12xy | 216.5xy+14y^2-5x=0 |
| x^2+3.5xy+y=y^2-xy+y            | x^2-y^2+4.5xy=0                    |
