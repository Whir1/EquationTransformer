﻿using NUnit.Framework;
using TechTalk.SpecFlow;

namespace EquationTransformer.Lib.Tests.SpecFlow
{
    [Binding]
    public class ConverterSteps
    {
        [Given("I have entered string equation (.*)")]
        public void GetParsing (string stringToParse)
        {
            ScenarioContext.Current.Set(stringToParse, "stringToParse");
        }

        [When ("I convert equation")]
        public void ParseString()
        {
            var stringToParse = ScenarioContext.Current.Get<string>("stringToParse");
            var converter = ScenarioContext.Current.Get<EquationConverter>();

            var result = converter.ConvertToCanonical(stringToParse);
            ScenarioContext.Current.Set(result, "result");

        }

        [Then ("the result should be (.*)")]
        public void AsserResult(string expected)
        {
            var result = ScenarioContext.Current.Get<string>("result");

            Assert.AreEqual(expected, result);

        }
    }
}