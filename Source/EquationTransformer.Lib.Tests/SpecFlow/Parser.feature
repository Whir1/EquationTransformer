﻿Feature: I want to test my parser

Background: 
	Given Create parser

@Integrate
Scenario Outline: ParseStrings
	When Parse string <parsingString>
	Then The result should be expected <expectedString>

Examples: 
| parsingString			 | expectedString		 |
| x=5					 | x=5					 |
| 5x^2=5				 | 5x^2=5				 |
| 7*8+8.5(5x^2)*y^3 = 10 | 7*8+8.5*5(x^2)(y^3)=10|
| 10x*23y-1.5xy+7y^2=5x-7y^2+12xy| 10*23xy-1.5xy+7y^2=5x-7y^2+12xy					 |
