﻿using System.Globalization;
using EquationTransformer.Lib.Parsers;
using TechTalk.SpecFlow;

namespace EquationTransformer.Lib.Tests.SpecFlow
{

    [Binding]
    public class ParserBackgroound
    {
        [Given("Create parser")]
        public void CreateParser()
        {
            EquationParser parser = new EquationParser(new SummandParser(CultureInfo.InvariantCulture));

            ScenarioContext.Current.Set(parser);
        }

        [Given("Create converter")]
        public void CreateConvarter()
        {
            var factory = new EquationConverterFactory();
            EquationConverter converter = factory.BuildEquationConverter(CultureInfo.InvariantCulture);

            ScenarioContext.Current.Set(converter);
        }
    }
}