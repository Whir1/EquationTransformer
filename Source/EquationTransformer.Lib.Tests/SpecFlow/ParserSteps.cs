﻿using EquationTransformer.Lib.Model;
using EquationTransformer.Lib.Parsers;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace EquationTransformer.Lib.Tests.SpecFlow
{
    [Binding]
    public class ParserSteps
    {
        [When("Parse string (.*)")]
        public void ParseString(string equationString)
        {
            var parser = ScenarioContext.Current.Get<EquationParser>();

            Equation actual = parser.Parse(equationString);

            ScenarioContext.Current.Set(actual);
        }

        [Then ("The result should be expected (.*)")]
        public void ThenResultShouldBeExpected(string expected)
        {
            Equation actual = ScenarioContext.Current.Get<Equation>();

            Assert.AreEqual(expected, actual.ToString());
        }
    }
}