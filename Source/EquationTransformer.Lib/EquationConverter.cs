﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquationTransformer.Lib.Model;
using EquationTransformer.Lib.Parsers;
using EquationTransformer.Lib.Processors;

namespace EquationTransformer.Lib
{
    public class EquationConverter
    {
        private readonly IParser<Equation> _parser;
        private readonly ICollection<IEquationProcessor> _processors;

        public EquationConverter(IParser<Equation> parser, ICollection<IEquationProcessor> processors)
        {
            _parser = parser;
            _processors = processors;
        }

        public string ConvertToCanonical(string input)
        {
            var equation = _parser.Parse(input);
            equation = _processors.Aggregate(equation, (current, equationProcessor) => equationProcessor.ProcessEquation(current));
            return equation.ToString();
        }
    }
}
