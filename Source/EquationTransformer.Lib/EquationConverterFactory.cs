﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using EquationTransformer.Lib.Parsers;
using EquationTransformer.Lib.Processors;

namespace EquationTransformer.Lib
{
    public class EquationConverterFactory
    {
        public EquationConverter BuildEquationConverter(CultureInfo culture)
        {
            var summandParser = new SummandParser(culture);
            var equationParser = new EquationParser(summandParser);

            var processors = new List<IEquationProcessor>
            {
                new PartMoveProcessor(),
                new SummandOptimizerProcessor(culture),
                new SummandReducerProcessor()
            };

            return new EquationConverter(equationParser, processors.OrderBy(c => c.Priority).ToList());
        } 
    }
}