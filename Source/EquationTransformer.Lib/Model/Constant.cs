﻿using System.Globalization;

namespace EquationTransformer.Lib.Model
{
    public class Constant : VariableBase
    {
        public float Value { get; }

        public Constant(float value) : this (value, CultureInfo.InvariantCulture)
        { }

        public Constant(float value, CultureInfo culture) : base(culture)
        {
            Value = value;
        }

        public override string ToString()
        {
            return Value.ToString(CultureInfo);
        }

        public override VariableType Variabletype => VariableType.Constant;
        public override bool IsComplex => false;
        public override bool IsNegative => Value < 0;
        public override float Power => -1;

        public static Constant operator *(Constant left, Constant right)
        {
            return new Constant(left.Value * right.Value, left.CultureInfo);
        }

        public static Constant operator +(Constant left, Constant right)
        {
            return new Constant(left.Value + right.Value, left.CultureInfo);
        }
    }
}