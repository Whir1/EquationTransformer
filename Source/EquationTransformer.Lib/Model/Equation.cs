﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EquationTransformer.Lib.Model
{
    public class Equation
    {
        public IReadOnlyCollection<Summand> LeftParts { get; }
        public IReadOnlyCollection<Summand> RightParts { get; }

        /// <exception cref="ArgumentException">right part or left part is null or empty</exception>
        public Equation(ICollection<Summand> leftParts, ICollection<Summand> rightParts)
        {
            if (leftParts == null || !leftParts.Any())
                throw new ArgumentException("leftPart is null or empty", nameof(leftParts));
            if (rightParts == null || !rightParts.Any())
                throw new ArgumentException("rightParts is null or empty", nameof(rightParts));

            LeftParts = leftParts.ToList();
            RightParts = rightParts.ToList();
        }

        public override string ToString()
        {
            return $"{FormatPart(LeftParts)}={FormatPart(RightParts)}";
        }

        private string FormatPart(IReadOnlyCollection<Summand> summands)
        {
            var sb = new StringBuilder();

            bool isFirst = true;

            foreach (var summand in summands.OrderByDescending(c => c.GetRank()))
            {
                if (!isFirst)
                {
                    string operand = GetOperand(summand);
                    sb.Append(operand);
                }
                sb.Append(summand);
                isFirst = false;
            }

            return sb.ToString();
        }

        private string GetOperand(Summand summand)
        {
            var firstVariable = summand.Variables.FirstOrDefault();
            if (firstVariable == null)
                return "";
            return firstVariable.IsNegative ? "" : Symbol.Plus.ToString();
        }
    }
}