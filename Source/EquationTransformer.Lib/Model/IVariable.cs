﻿namespace EquationTransformer.Lib.Model
{
    public interface IVariable
    {
        VariableType Variabletype { get; }

        bool IsComplex { get; }

        bool IsNegative { get; }
        float Power { get;}
    }
}