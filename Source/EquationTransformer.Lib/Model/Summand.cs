﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EquationTransformer.Lib.Model
{
    public class Summand
    {
        private readonly ICollection<IVariable> _variables;

        /// <exception cref="ArgumentException">variables is null or empty</exception>
        public Summand(ICollection<IVariable> variables)
        {
            if(variables == null || !variables.Any())
                throw new ArgumentException("variables is null or empty", nameof(variables));

            _variables = variables.OrderBy(c => c.Variabletype).ToList();
        }

        public IReadOnlyCollection<IVariable> Variables => _variables.ToList();

        public float GetRank()
        {
            return _variables.Max(c => c.Power);
        }

        public override string ToString()
        {
            if (_variables == null || !_variables.Any())
            {
                return "0";
            }

            var sb = new StringBuilder();

            var groupedVariables = _variables.GroupBy(c => c.Variabletype).ToList();

            var constantVariables = groupedVariables.FirstOrDefault(c => c.Key == VariableType.Constant);
            var variables = groupedVariables.FirstOrDefault(c => c.Key == VariableType.Variable);

            sb.Append(PrintConstants(constantVariables, variables));
            sb.Append(PrintVariables(variables));

            return sb.ToString();
        }

        private string PrintVariables(IGrouping<VariableType, IVariable> variables)
        {
            var sb = new StringBuilder();
            if (variables != null && variables.Any())
            {
                var simpleVars = variables.Where(c => c.IsComplex == false);
                sb.Append(string.Join("", simpleVars));

                IEnumerable<IVariable> complexVars = variables.Where(c => c.IsComplex).ToList();
                if (complexVars.Count() == 1)
                {
                    sb.Append(complexVars.First());
                }
                else
                {
                    foreach (var complexVariable in complexVars)
                    {
                        sb.AppendFormat("{0}{1}{2}", Symbol.BracketLeft, complexVariable, Symbol.BracketRight);
                    }
                }
            }
            return sb.ToString();
        }

        private string PrintConstants(IGrouping<VariableType, IVariable> constantVariables, IGrouping<VariableType, IVariable> vars)
        {
            if (vars!= null && vars.Any())
            {
                if (constantVariables!= null && constantVariables.Count() == 1)
                {
                    var constant = constantVariables.First() as Constant;
                    if(Math.Abs(constant.Value) - 1 < Constants.Epsilon && constant.IsNegative)
                    {
                        return "-";
                    }
                }
            }
            if (constantVariables != null && constantVariables.Any())
                return string.Join(Symbol.Multiplication.ToString(), constantVariables);
            return "";
        }

        public bool IsSimilarTo(Summand other)
        {
            IEnumerable<IVariable> thisVariables = _variables.Where(c => c.Variabletype == VariableType.Variable);
            IEnumerable<IVariable> othervariables = other._variables.Where(c => c.Variabletype == VariableType.Variable);

            return thisVariables.SequenceEqual(othervariables);
        }

        public static Summand operator +(Summand left, Summand right)
        {
            if (!left.IsSimilarTo(right))
                throw new InvalidOperationException("Cant plus not similar summands");

            var leftConstant = left.Variables.SingleOrDefault(c => c.Variabletype == VariableType.Constant) as Constant;
            var rightConstant = right.Variables.SingleOrDefault(c => c.Variabletype == VariableType.Constant) as Constant;


            var constant = (leftConstant?.Value ?? 1f) + (rightConstant?.Value ?? 1f);

            var leftVariables = left.Variables.Where(c => c.Variabletype == VariableType.Variable);

            var variables = new List<IVariable>
            {
                new Constant(constant)
            };
            variables.AddRange(leftVariables);
            return new Summand(variables);
        }
    }
}