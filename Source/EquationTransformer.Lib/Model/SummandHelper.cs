﻿using System.Collections.Generic;

namespace EquationTransformer.Lib.Model
{
    public static class SummandHelper
    {
        public static Summand GetZeroSummand()
        {
            return new Summand(new List<IVariable>() {new Constant(0)});
        }
    }
}