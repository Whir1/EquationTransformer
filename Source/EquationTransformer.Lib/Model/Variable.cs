﻿using System;
using System.Globalization;

namespace EquationTransformer.Lib.Model
{
    public class Variable : VariableBase
    {
        public char Name { get; }
        public override float Power { get; }

        public Variable(char name, float power = 1) : this(name, CultureInfo.InvariantCulture, power)
        {}

        public Variable(char name, CultureInfo culture, float power = 1) : base(culture)
        {
            Name = name;
            Power = power;
        }


        #region EqualByResharper
        protected bool Equals(Variable other)
        {
            return Name == other.Name && Power.Equals(other.Power) && Variabletype.Equals(other.Variabletype);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Variable) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Name.GetHashCode()*397) ^ Power.GetHashCode();
            }
        }

        #endregion
        public override string ToString()
        {
            if (Math.Abs(Power) < Constants.Epsilon)
            {
                return "1";
            }
            return Math.Abs(Power - 1) < Constants.Epsilon 
                ? Name.ToString(CultureInfo) 
                : $"{Name}{Symbol.Power}{Power}";
        }

        public override VariableType Variabletype => VariableType.Variable;
        public override bool IsComplex => Math.Abs(Power) > Constants.Epsilon && Math.Abs(Power - 1) > Constants.Epsilon;
        public override bool IsNegative => false;

        public static Variable operator *(Variable left, Variable right)
        {
            if(left.Name != right.Name)
                throw new ArgumentException("Cant apply multiplication operator to var with different names");
            return new Variable(left.Name, left.CultureInfo, left.Power + right.Power);
        }
    }

}