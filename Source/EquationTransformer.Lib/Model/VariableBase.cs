﻿using System.Globalization;

namespace EquationTransformer.Lib.Model
{
    public abstract class VariableBase : IVariable
    {
        protected CultureInfo CultureInfo { get; }

        protected VariableBase(CultureInfo cultureCultureInfo)
        {
            CultureInfo = cultureCultureInfo;
        }

        public abstract VariableType Variabletype { get; }
        public abstract bool IsComplex { get; }
        public abstract bool IsNegative { get; }
        public abstract float Power { get; }
    }
}