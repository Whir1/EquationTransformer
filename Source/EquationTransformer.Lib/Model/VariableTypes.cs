﻿namespace EquationTransformer.Lib.Model
{
    public enum VariableType
    {
        Constant = 0,
        Variable = 1
    }
}