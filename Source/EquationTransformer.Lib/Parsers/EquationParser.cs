﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using EquationTransformer.Lib.Model;


namespace EquationTransformer.Lib.Parsers
{
    public class EquationParser : IParser<Equation>
    {
        private static readonly char[] Operands =
{
            Symbol.Plus,
            Symbol.Minus,
            Symbol.Equal
        };

        private readonly IParser<Summand> _summandParser;

        public EquationParser(IParser<Summand> summandParser)
        {
            _summandParser = summandParser;
        }

        public Equation Parse(string stringToParse)
        {
            stringToParse = stringToParse.Replace(" ", "");
            var findEq = false;
            var sb = new StringBuilder();

            var leftPart = new List<Summand>();
            var rightPart = new List<Summand>();
            var prevChar = Symbol.Plus; 
            foreach (char c in stringToParse)
            {
                if (c == Symbol.Equal)
                {
                    findEq = true;
                }
                if (Operands.Contains(c) && !Operands.Contains(prevChar))
                {
                    if (findEq && c != Symbol.Equal)
                    {
                        rightPart.Add(_summandParser.Parse(sb.ToString()));
                    }
                    else
                    {
                        leftPart.Add(_summandParser.Parse(sb.ToString()));
                    }
                    sb.Clear();
                }
                if (c != Symbol.Equal)
                {
                    sb.Append(c);
                }
                prevChar = c;
            }
            rightPart.Add(_summandParser.Parse(sb.ToString()));
            return new Equation(leftPart, rightPart);

        }
    }
}