﻿namespace EquationTransformer.Lib.Parsers
{
    public interface IParser<out T>
    {
        T Parse(string stringToParse);
    }
}