﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using EquationTransformer.Lib.Model;
using EquationTransformer.Tokenizer;
using static System.Char;

namespace EquationTransformer.Lib.Parsers
{
    public class SummandParser : IParser<Summand>
    {
        private TokenReader _tokenReader;

        public SummandParser():this(CultureInfo.InvariantCulture)
        { }
        public SummandParser(CultureInfo culture)
        {
            _tokenReader = new TokenReader(culture);
        }

        /// <exception cref="TokenReaderException">Condition.</exception>
        public Summand Parse(string stringToParse)
        {
            var formattedSummand = FormatSummand(stringToParse);

            List<Token> tokens = _tokenReader.Read(formattedSummand);
            return ConvertTokensToSummand(tokens);
        }

        private string FormatSummand(string stringToParse)
        {
            var sb = new StringBuilder();
            var prevChar = ' ';

            foreach (var c in stringToParse)
            {
                if ((IsLetter(prevChar) && IsLetter(c)) || (IsDigit(prevChar) && (IsLetter(c) || c== Symbol.BracketLeft)))
                {
                    sb.Append('*');
                }
                if (prevChar == Symbol.Minus && IsLetter(c))
                {
                    sb.Append("1*");
                }
                sb.Append(c);
                prevChar = c;
            }
            return sb.ToString();
        }

        private Summand ConvertTokensToSummand(List<Token> tokens)
        {
            var variables = new List<IVariable>();

            if (tokens.Count == 1)
            {
                var token = tokens.First();
                switch (token.TokenType)
                {
                        case TokenType.Numeric:
                            variables.Add(new Constant(float.Parse(token.Value)));
                        break;
                        case TokenType.Text:
                            variables.Add(new Variable(token.Value[0]));
                        break;
                }
                return new Summand(variables);
            }

            for (int i=0; i < tokens.Count; i++)
            {
                var current = tokens[i];
                switch (current.TokenType)
                {
                    case TokenType.Numeric:
                        if (i == tokens.Count -1)
                        {
                            variables.Add(new Constant(float.Parse(current.Value)));
                        }
                        if (i < tokens.Count - 1)
                        {
                            Token nтextToken = tokens[++i];
                            if (nтextToken.TokenType == TokenType.Operation)
                            {
                                variables.Add(new Constant(float.Parse(current.Value)));
                            }
                        }
                        break;
                    case TokenType.Text:
                        if (i == tokens.Count - 1)
                        {
                            variables.Add(new Variable(current.Value[0]));
                        }
                        else
                        {
                            Token nextToken = tokens[++i];
                            if (nextToken.TokenType == TokenType.Operation)
                            {
                                variables.Add(new Variable(current.Value[0]));
                            }
                            if (nextToken.TokenType == TokenType.Power)
                            {
                                Token powerToken = tokens[++i];
                                variables.Add(new Variable(current.Value[0], float.Parse(powerToken.Value)));
                            }

                        }
                        break;
                    case TokenType.LeftBracket:
                    case TokenType.RightBracket:
                        continue;
                }

            }

            var result = new Summand(variables);
            return result;
        }
    }
}