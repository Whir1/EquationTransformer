﻿using EquationTransformer.Lib.Model;

namespace EquationTransformer.Lib.Processors
{
    public interface IEquationProcessor
    {
        Equation ProcessEquation(Equation equation);

        int Priority { get; }
    }
}