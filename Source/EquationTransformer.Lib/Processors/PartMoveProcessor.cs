﻿using System.Collections.Generic;
using EquationTransformer.Lib.Model;

namespace EquationTransformer.Lib.Processors
{
    public class PartMoveProcessor : IEquationProcessor
    {
        public Equation ProcessEquation(Equation equation)
        {
            return MovePartToLeft(equation.LeftParts, equation.RightParts);
        }

        private Equation MovePartToLeft(IReadOnlyCollection<Summand> leftParts, IReadOnlyCollection<Summand> rightParts)
        {
            var part = new List<Summand>();
            part.AddRange(leftParts);
            foreach (var summand in rightParts)
            {
                var variables = new List<IVariable>(summand.Variables);
                var constant = new Constant(-1);
                variables.Add(constant);
                part.Add(new Summand(variables));
            }

            return new Equation(part, new[] { SummandHelper.GetZeroSummand()});
        }

        public int Priority => 1;
    }
}