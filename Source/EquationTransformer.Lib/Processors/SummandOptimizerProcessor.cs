﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EquationTransformer.Lib.Model;

namespace EquationTransformer.Lib.Processors
{
    public class SummandOptimizerProcessor : IEquationProcessor
    {
        private readonly CultureInfo _culture;

        public SummandOptimizerProcessor():this(CultureInfo.InvariantCulture)
        { }
        public SummandOptimizerProcessor(CultureInfo culture)
        {
            _culture = culture;
        }

        public Equation ProcessEquation(Equation equation)
        {
            ICollection<Summand> optimizedLeft = OptimizePart(equation.LeftParts);
            ICollection<Summand> optimizedRight = OptimizePart(equation.RightParts);
            return new Equation(optimizedLeft, optimizedRight);
        }

        private ICollection<Summand> OptimizePart(IReadOnlyCollection<Summand> summands)
        {
            return summands.Select(summand => OptimizeParts(summand.Variables))
                           .Where(c => c != null)
                           .ToList();
        }

        private Summand OptimizeParts(IReadOnlyCollection<IVariable> variables)
        {
            var result = new List<IVariable>();
            var groupedVariables = variables.GroupBy(c => c.Variabletype).OrderBy(c => c.Key);

            var constants = groupedVariables.FirstOrDefault(c => c.Key == VariableType.Constant);
            var vars = groupedVariables.FirstOrDefault(c => c.Key == VariableType.Variable);

            if (constants != null && constants.Any())
            {
                Constant constant = GroupConstants(constants.ToList());
                if (Math.Abs(constant.Value) < Constants.Epsilon)
                {
                    return SummandHelper.GetZeroSummand();
                }

                if(Math.Abs(constant.Value - 1) > Constants.Epsilon || vars == null || !variables.Any())
                    result.Add(constant);
            }
            if (vars != null && vars.Any())
            {
                var optimizedVariables = GroupVariables(vars.ToList());
                result.AddRange(optimizedVariables);
            }

            return new Summand(result);
        }

        private ICollection<IVariable> GroupVariables(ICollection<IVariable> variables)
        {
            List<Variable> typedVariables = variables.OfType<Variable>().ToList();
            IEnumerable<IGrouping<char, Variable>> groupedVars = typedVariables.GroupBy(c => c.Name);
            IEnumerable<Variable> multiplicatedVariables = groupedVars.Select(groupedVar => groupedVar.Aggregate((current, c) => current*c));

            var result = new List<IVariable>();
            foreach (Variable multiplicatedVariable in multiplicatedVariables)
            {
                if (Math.Abs(multiplicatedVariable.Power) > Constants.Epsilon)
                {
                    result.Add(multiplicatedVariable);
                }
            }
            return result;
        }

        private Constant GroupConstants(List<IVariable> constantVariables)
        {
            return constantVariables.OfType<Constant>().Aggregate((current, c) => current*c);
        }

        public int Priority => 2;
    }
}