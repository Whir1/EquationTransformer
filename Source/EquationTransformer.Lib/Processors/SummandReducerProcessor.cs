﻿using System;
using System.Collections.Generic;
using System.Linq;
using EquationTransformer.Lib.Model;

namespace EquationTransformer.Lib.Processors
{
    public class SummandReducerProcessor : IEquationProcessor
    {
        public Equation ProcessEquation(Equation equation)
        {
            List<Summand> optimizedLeft = ReducePart(equation.LeftParts);
            List<Summand> optimizedRight = ReducePart(equation.RightParts);
            return new Equation(optimizedLeft, optimizedRight);
        }

        private List<Summand> ReducePart(IReadOnlyCollection<Summand> summandToReduce)
        {
            var result = new List<Summand>();
            foreach (var summand in summandToReduce)
            {
                var similar = result.FirstOrDefault(c => c.IsSimilarTo(summand));
                if (similar == null)
                {
                    result.Add(summand);
                }
                else
                {
                    var index = result.FindIndex(c => c.IsSimilarTo(summand));
                    result[index] += summand;

                    var indexSummand = result[index];
                    var constVar = indexSummand.Variables.FirstOrDefault(c => c.Variabletype == VariableType.Constant);

                    var constant = constVar as Constant;
                    if (constant != null && Math.Abs(constant.Value) < Constants.Epsilon)
                    {
                            result.RemoveAt(index);
                    }
                }
            }
            

            return result;
        }

        public int Priority => 3;
    }
}