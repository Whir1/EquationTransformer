﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;

namespace EquationTransformer.Tokenizer.Tests
{
    [TestFixture]
    public class TokenReaderFixture
    {
        [Test]
        [TestCaseSource("EquationTestCases")]
        public void WhenReadTokens_ThenReturnsExpectedResult(List<Token> expected, string giwen)
        {
            var _target = new TokenReader();

            var actual = _target.Read(giwen);

            actual.ShouldBeEquivalentTo(expected);
        }

        private static object[] EquationTestCases = {
                new object[] {
                    new List<Token>
                    {
                        new Token {TokenType = TokenType.Numeric, Value = "5"},
                        new Token {TokenType = TokenType.Operation, Value = "*"},
                        new Token {TokenType = TokenType.Text, Value = "x"},
                        new Token {TokenType = TokenType.Operation, Value = "*"},
                        new Token {TokenType = TokenType.Text, Value = "y"},
                        new Token {TokenType = TokenType.Power, Value = "^"},
                        new Token {TokenType = TokenType.Numeric, Value = "2"},
                    }, "5*x*y^2"
                },
                new object[] {
                    new List<Token>
                    {
                        new Token {TokenType = TokenType.Numeric, Value = "5.2"},
                        new Token {TokenType = TokenType.Operation, Value = "*"},
                        new Token {TokenType = TokenType.Text, Value = "x"},
                    }, "5.2*x"
                },
                new object[] {
                    new List<Token>
                    {
                        new Token {TokenType = TokenType.Numeric, Value = "5"},
                        new Token {TokenType = TokenType.Operation, Value = "*"},
                        new Token {TokenType = TokenType.Text, Value = "x"},
                        new Token {TokenType = TokenType.Operation, Value = "*"},
                        new Token {TokenType = TokenType.Text, Value = "y"},
                        new Token {TokenType = TokenType.Power, Value = "^"},
                        new Token {TokenType = TokenType.Numeric, Value = "-2"},
                    }, "5*x*y^-2"
                },
                new object[] {
                    new List<Token>
                    {
                        new Token {TokenType = TokenType.Numeric, Value = "52"},
                        new Token {TokenType = TokenType.Operation, Value = "*"},
                        new Token {TokenType = TokenType.Text, Value = "x"},
                        new Token {TokenType = TokenType.Operation, Value = "*"},
                        new Token {TokenType = TokenType.LeftBracket, Value = "("},
                        new Token {TokenType = TokenType.Text, Value = "y"},
                        new Token {TokenType = TokenType.Power, Value = "^"},
                        new Token {TokenType = TokenType.Numeric, Value = "-2"},
                        new Token {TokenType = TokenType.RightBracket, Value = ")"},
                        new Token {TokenType = TokenType.Operation, Value = "*"},
                        new Token {TokenType = TokenType.LeftBracket, Value = "("},
                        new Token {TokenType = TokenType.Text, Value = "z"},
                        new Token {TokenType = TokenType.Power, Value = "^"},
                        new Token {TokenType = TokenType.Numeric, Value = "2"},
                        new Token {TokenType = TokenType.RightBracket, Value = ")"},
                    }, "52*x*(y^-2)*(z^2)"
                },
        };
    }
}
