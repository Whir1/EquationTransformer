﻿namespace EquationTransformer.Tokenizer
{
    public class Token
    {
        public TokenType TokenType;

        public string Value;
    }
}