﻿namespace EquationTransformer.Tokenizer
{
    public enum TokenType
    {
        Numeric,
        Power,
        Text,
        Operation,
        LeftBracket,
        RightBracket,
    }
}