﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EquationTransformer.Lib;
using NLog;

namespace EquationTransformer
{
    class Program
    {
        private static Logger _logger;

        static void Main(string[] args)
        {
            _logger = LogManager.GetCurrentClassLogger();

            var factory = new EquationConverterFactory();
            EquationConverter converter = factory.BuildEquationConverter(CultureInfo.InvariantCulture);

            if (args.Length == 0)
            {
                ReadFromConsole(converter);
            }
            if (args.Length == 1)
            {
                ConvertFile(Path.GetFullPath(args[0]), converter);
            }

        }

        private static void ConvertFile(string inputFilePath, EquationConverter converter)
        {
            var outFilePath = inputFilePath + ".out";
            using (var file = new StreamReader(inputFilePath))
            {
                using (var outFile = new StreamWriter(outFilePath, false))
                {
                    string line;
                    while ((line = file.ReadLine()) != null)
                    {
                        try
                        {
                            var result = converter.ConvertToCanonical(line);
                            outFile.Write(result);
                        }
                        catch (Exception exc)
                        {
                            _logger.Error(exc, "stringEquation:{0}", line);
                            outFile.WriteLine("with line{0} Something was wrong, see log file for details", line);
                        }
                        finally
                        {
                            outFile.Write(Environment.NewLine);
                        }
                    }
                }
            }
        }

        private static void ReadFromConsole(EquationConverter converter)
        {
            do
            {
                string stringEquation = Console.ReadLine();
                try
                {
                    string converted = converter.ConvertToCanonical(stringEquation);
                    Console.WriteLine(converted);
                }
                catch (Exception exc)
                {
                    _logger.Error(exc, "stringEquation:{0}", stringEquation);
                    Console.WriteLine("Something was wrong, see log file for details");
                }
            } while (true);
        }
    }
}
